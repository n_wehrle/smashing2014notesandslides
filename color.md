Lea Verou - Engineering Color on the Web
========================================
(Tue - 1115AM)

* `rgb(0-255)`
* `rgb(percentages)`
* `hsl(angle in deg, perc, perc)`
* `filter: hue-rotate(240deg);`
* `rgba(,,,)` a stands for alpha
* `hsla(,,,)`
* in CSS: `currentColor`

Level 4
-------
* `#f3a0`4 digits, last alpha
* `#ff33aa00`8 digits, last 2 alpha
* color adjusters: red(), hue(), tint(), shade() ...
* [myth.io Polyfill](myth.io)
* `hwb()` like in Photoshop