Guy Podjarny - Putting your images on a diet
============================================
(Tue - 2:40pm)

* focus: image compression
* PNG Tools: PNGCrush, OptiPNG

JPG optimization
----------------
* most traffic comes from jpeg images (70%?)
* quality level != percentage
* strip metadata
* Tools: jpegtran, ImageMagick, jpegrescan, ImageOptim
* in general: prefer progressive
* why not in wide use: browser do not render progressivly
* for retina: double size, lower quality

Future
------
* WebP by Google
  * 16-42% smaller than png
  * 25-37% smaller than jpeg
  * Browser support: Chrome, Android, Opera
* JPEG XR by Microsoft
  * only IE10+
  * slightly bigger than WebP
* JPEG 2000
  * 30% smaller than jpeg
  * support on iOS, Mac Safari

Selective Delivery
------------------
* Accept header
* same url, different resource

What to use
-----------
* tiny images: GIF
* small images: PNG
* where possible: prefer JPEG to PNG
* new formats where possible


