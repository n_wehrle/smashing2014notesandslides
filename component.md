Wilson Page - The component approach
====================================
(Wed - 1:50pm)

> Break pages into chunks...

* markup, styling, behaviour (API and events)
* no need to use a framework to use components
* example for a compoment framework: *Fruit Machine*
* Events: something has happened
* API: cause something to happen
* Library FastDOM: batch DOM operations
* Concept of layout boundary to improve performance
* define width, height and overflow to create a layout boundary
* there is a browser plugin: boundarizr (?)
* Solved: mapping css breakpoints to JS breakpoints by handling media events: `window.matchMedia()`

[Slides]()