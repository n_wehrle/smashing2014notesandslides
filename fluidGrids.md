Nathan Ford - Connecting content, layout and device through Fluid Grids
===========================================================
(Wed 12:10 pm)

## Foreword
* Design talk
* Maker of gridset

## Workflow
* start with content
* construct your layout from the content out
* bind the content to the device

## Ratios
* 12 Orthogons (a list of ratios that work)
* [modularscale.com](modularscale.com)
* ratios can be used directly in flexbox

## Usage
* fluid grids define relationships in the content
* fluid grids are modular

[gridsetapp.com](gridsetapp.com)

