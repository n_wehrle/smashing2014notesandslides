Jon Hicks - Design Process
===========================
(Wed - 9:00am)

Why do I need my own icon font?
-------------------------------
* not the right size
* not the right style
* too many spare icons
* not the right icons

Designing Icons
---------------
* Follow conventions (house instead of welcome mat for *Home*)
* Local knowledge (owl in the west = wisdom, in the east = stupidity)
* don't get to fancy (simple home instead of manor)
* final export format: svg
* work on all icons together (to be able to compare)
* Find a visual balance (don't use the whole available space for heavy icons)
* always use a label in addition to the icon!
* Export SVG1.1, 1 decimal place
* compress svg: svgo-gui

Iconfonts or SVG?
-----------------
* well known: sprites with `background-position`
* why iconfont
  * one small file
  * accessible
  * scalable
  * can be styled in css
  * no sprites
  * support in IE4(!)
* why not
  * fiddly process
  * no meaning
  * only monochrome
  * lack of font-face support
  * font may not load
  * rendering inconsistencies  
* why SVG
  * less hassle
  * support
  * no sprites
  * multiple colors
  * stylable in css (reuse the svg: use xlink attribute)
  * animations
* why not
  * file size
* tools
  * drag and drop on [grumpicon](http://grumpicon.com) (there is a cli version, too)
    * creates loader.js
    * creates png fallback
    * create css
    * there is a grunt plugin called grunticon
  * [iconizr](http://iconizr.com)
   
