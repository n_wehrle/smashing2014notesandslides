Marko Dugonjic - Responsive Web Typography
==========================================
(Wed - 2:40pm)

## Variables
* reading distance
* viewport width
* viewport height
    * line height
    * letter spacing
    * word spacing
* content hierarchy
    * font size .. [calculate](modularsize.com)
    * style headlines different
    * indent paragraphs
* pixel density
* orientation
    * FitText (JS-Library)
    * media query, than font width
* screen aspect ratio

> Breakpoints should not be dictated by devices, but by content.

[Slides](#)



