Lea's CSS Workshop
==================
* starting with plain css, level of abstraction
* calc function, knows viewport dimensions
* calc knows more than a preprocessor
* [css-workshop](http://lea.verou.me/css-workshop/)

Fallbacks
---------
* `@supports` instead of Modernizr
* DIY: `element in document.body.style`
* test for prefixes, too
* set property on style, try/catch

Colors
------
* many css properties inherit the color property
* `currentColor` is a css variable
* support for semitransparent colors
* `#ff800099`- last 2 digits for transparency
* `#f709`- last digit for transparency
* `color`-function: shade, blend
* more info: color level 4

Border Radius
-------------
* if `width` is smaller than `border-radius`, border-radius will be adapted
* ellipse, half ellipse, quarter ellipse
* in the making: `corner-shape` (produces any shape you like in combination with border-radius)

Box Shadow
----------
* multiple shadows possible
* can act as borders
* shadow follows border-radius

Text Shadow
-----------
* multiple shadows possible

Backgrounds
-----------
* `background-origin: content-box` aligns background images with texts that have padding
* `background-clip`
* `background-size: contain` fit into frame, no resize
* `background-size: cover` fit into frame, resize one dimension

Gradients
---------
* gradient on background, size smaller = stripes
* gradient in ff from white to transparent goes through gray
* workaround: use transparent white `hsla()`
* `repeating-linear-gradient` for stripes
* `radial-gradient`
* [Showcase](http://dabblet.com/gist/9594012)

Translations
------------
* order matters
* `transform-origin`
* transformation on parent makes child element's position relative to parent

Transitions
-----------
* can be delayed
* multiple transitions in a sequence possible (using delays)
* only available if you can calculate intermediate states
* visibility cannot be animated, but can be emulated by `transform: scale(0)`
* `cubic-bezier`: define the way an object accelerates and breaks through a magic formula

Animations
----------
* `@keyframes`
* animate-direction is able to reverse all, even the timing function
* if one property (within a ahorthand-statement) is not animatable, the whole statement will become ignored
* workaround: do not use shorthand properties, but extract single properties
* `steps` for frame by frame animation
* steps can animate pngs (alpha tranparency)

Layout
------
* `box-sizing`
* `calc`-function (great support, beside IE8)
* `column-count`
* `column-width`
* `column-gap`
* `column-rule`
* column support is great, beside IE8
* `display: flex`
* `flex-direction: column-reverse`








 
