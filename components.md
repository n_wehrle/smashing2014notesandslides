Addy Osmani - Web Components
============================
(Tue 1000AM)

* all about encapsulation
* emerging standard

Elements are
------------
* configurable
* composable
* programmable

Web Components consist of
-------------------------
* Templates
* Custom Elements
* Shadow DOM
* HTML Imports

Usage
-----
* `document.registerELement()`
* `Object.create(HTMLElement.prototype ..)`

CSS-selectors
-------------
* `my-element /shadow/ h2`
* `my-element /shadow-deep/ h2`

For now
-------
* Library *Polymer*
* use and create elements
> Everything is an element
* Element for local storage available
* Element for ajax requests
* Extends media queries for *RWD*
* in Polymer elements are created declarative (differs from standard but removes boilerplate)
* polymer uses templates in moustache-like language
* declarative event handlers
* SEO friendly
* [Library of custom elements](customelements.io)











