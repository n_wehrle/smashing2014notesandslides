Zoe M. Gillenwater - Leveling up with Flexbox
=============================================
(Wed - 11:15am)

* feed less capable browsers a different experience
* use flexbox now as progressive enhancement
* syntax is solid
* current syntax: `display:flex`
* flexy boxes code generator shows old and new syntax
* autoprefixer does it automatically
* `flex-direction`
* `flex-wrap`
* `flex-flow` is shorthand notation
* example page: s'mores builder
* `justify-content: space-between`
* `align-items: stretch`
* fallback: `display:table`
* fallbacks may coexists, sometimes need to be isolated using Modernizr
* `flex-grow, flex-shrink. flex-basis` define proportions
* shorthand `flex 1 0 200px`

## Advantages
* less CSS
* less media queries
* flex adjusts for margins
* flex adjusts for quantity of items (no fancy classnames for 3,4,5 columns)

## Warning
* flex-basis is counted into available space
* example: 1,1,2 on a 50px area means a width of `(50 - sum(base)) / 4 * grow + base` is the width of each component
* order property does not influence tab-order yet! (beside firefox)

## Order of elements
* use natural order on desktop
* use flexbox order attribute on mobile
* support is great on mobile

## Flexbox enhancement ideas
* equally spaced items
* pinned items
* full-width forms
* reorder content blocks

[Presentation](zomigi.com/blog/leveling-up-with-flexbox)





