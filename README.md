# SmashingConf 2014 (Oxford)
In April 2014 I attended the SamshingConf 2014 in Oxford. These are my notes in Markdown. I reused the notes in my summarizing presentation for adesso Switzerland. The presentation is also here on Bitbucket.

## Topics
- Web Components
- Typography
- Responive Web Design
- UX, UI, Psychology
- New Image Formats
- Flexbox and CSS Grids