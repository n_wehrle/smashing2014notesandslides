Fabio Carneiro - Responsive Email Design
========================================
(Tue - 4:00pm)

* 90% own a cellphone
* 60%+ use it to check email
* outdated clients
* gmail - bad client, inline css, no media queries
* outlook 2007: render engine is word (!)
* many clients don't support max-width
* design is not the same as for websites
  * no navigation needed
  * no big footer
  * not many links

Make it responsive
------------------
* modular blocks
* mobile first
* table-based (!)
* conditional comments
* multicolumn to singlecolum: media query, than td to blocklevel
* aligned tables
* Web Resource: MailChimp Email Template Reference