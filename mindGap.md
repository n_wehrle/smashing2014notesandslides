Josh Clark - Mind the gap
=========================
Designing in the space between devices
--------------------------------------
(Tue - 6:00pm)

* 21 device changes for an adult per evening
* 90% accomplish tasks across multiple screens
* simultaneous: same task, same time
* in a sequence: start here, continue there
* gap
  * between different devices (hack: mail to yourself?)
  * between different behaviours (ui)
* WebRTC, Websockets

> Design for people, not for the screen

Device2Device (physical) interaction
------------------------------------
* example: kinect, take screenshot with gesture, touch smartphone to save
* challenge for imagination, not tech
* Internet of Things

> Everything that can be hacked, will be hacked
   